import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

let model = {clicks : 0};

function render() {
  ReactDOM.render(
    <App clickcounter={model.clicks} onClick={() => {model.clicks += 1; render();}}/>,
    document.getElementById('root')
  );
}

render();

